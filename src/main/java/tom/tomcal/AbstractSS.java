package tom.tomcal;


import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;


public abstract class AbstractSS {
    private final String program;
    private final PrintStream out;
    private final Scanner in = new Scanner(System.in);
    private final Collection<Command> commands = new ArrayList<>();

    private String prompt = "ss: ";


    protected AbstractSS(String program, String banner) {
        this.program = program;
        this.out = System.out;

        println(banner);
        println();

        return;
    }


    public void cmdHelp(String[] args) {
        boolean unavailable = false;

        if (args.length == 1) {
            println("Available " + this.program + " requests:\n");
            for (Command command: this.commands) {
                if (!command.isAvailable()) {
                    unavailable = true;
                    continue;
                }

                int ll = print(command.getName());
                for (String s : command.getAbbrevs()) {
                    ll += print(", " + s);
                }
                
                while (ll < 40) {
                    ll += print(" ");
                }
                
                println(command.getDescription());
            }
        }

        if (unavailable) {
            println();
            println("Unavailable " + this.program + " requests:\n");
            for (Command command: this.commands) {
                if (command.isAvailable()) {
                    continue;
                }

                int ll = print(command.getName());
                for (String s : command.getAbbrevs()) {
                    ll += print(", " + s);
                }
                
                while (ll < 40) {
                    ll += print(" ");
                }
                
                println(command.getDescription());
            }
        }

        return;
    }

    
    public void cmdQuit(String[] args) {
        System.exit(0);
    }


    protected void println(String text) {
        this.out.println(text);
        return;
    }


    protected void println() {
        this.out.println();
        return;
    }


    protected int print(String text) {
        this.out.print(text);
        return (text.length());
    }


    protected int print(Object o) {
        return (print(o.toString()));
    }


    protected void println(Object o) {
        println(o.toString());
        return;
    }


    protected void setPrompt(String prompt) {
        this.prompt = prompt;
        return;
    }

    
    protected void addCommand(String name, String[] abbrevs, String methodName, String description) {
        Method method;
        try {
            method = this.getClass().getMethod(methodName, String[].class);            
        } catch (NoSuchMethodException nsme) {
            throw new RuntimeException(methodName + " is not implemented");
        }

        this.commands.add(new Command(name, abbrevs, method, description));
        return;                   
    }

   
    protected final void run() {
        while (true) {
            print(this.prompt);
            dispatch(this.in.nextLine());
        }
    }
            
     
    protected Command findCommand(String s) {
        for (Command c : this.commands) {
            if (c.getName().equals(s)) {
                return (c);
            }
            
            if (c.getAbbrevs().contains(s)) {
                return (c);
            }
        }

        return (null);
    }

    
    private void dispatch(String line) {
        String[] tokens = line.replaceAll("\n", "").split(" ");
        
        if (tokens.length == 0) {
            return;
        }

        if (tokens[0].equals("?") || tokens[0].equals("help")) {
            cmdHelp(tokens);
            return;
        }

        Command command = findCommand(tokens[0]);
        if (command == null) {
            print("Unknown request \"");
            print (tokens[0]);
            println ("\".  Type \"?\" for a request list.");
            return;
        }

        try {
            command.getMethod().invoke(this, (Object) tokens);
        } catch (IllegalAccessException iae) {
            println(iae.getMessage());
        } catch (InvocationTargetException ite) {
            ite.printStackTrace();
        }

        return;
    }


    class Command {
        private final String name;
        private final String description;
        private final Collection<String> abbrevs = new ArrayList<>();
        private final Method method;
        private boolean available;

        
        Command(String name, String[] abbrevs, Method method, String description) {
            this.name = name;
            this.description = description;
            this.method = method;
            this.abbrevs.addAll(Arrays.asList(abbrevs)); 
            this.available = true;

            return;
        }

        
        public String getName() {
            return (this.name);
        }


        public String getDescription() {
            return (this.description);
        }


        public Collection<String> getAbbrevs() {
            return (this.abbrevs);
        }


        public Method getMethod() {
            return (this.method);
        }


        public boolean isAvailable() {
            return (this.available);
        }

        
        protected void available() {
            this.available = true;
        }


        protected void unavailable() {
            this.available = false;
        }
    }
}