/* Google calendar... look out! */

package tom.tomcal;

import org.osid.*;
import org.osid.calendaring.*;
import org.osid.ontology.*;
import org.osid.orchestration.*;

import org.osid.id.IdList;
import org.osid.resource.Resource;
import org.osid.resource.ResourceList;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.types.search.StringMatchTypes;
import net.okapia.osid.jamocha.id.id.MutableIdList;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.now;


public final class TomCal
    extends AbstractSS {

    private OsidRuntimeManager runtime;
    private CalendaringManager calManager;
    private OntologyManager ontologyManager;
    private Calendar calendar;


    TomCal() {
        super("tomcal", "Tomcal version 0.1. Type '?' for a list of commands.");
        setPrompt("tomcal: ");

        addCommand("load", new String[] {}, "cmdLoadOsidProvider", "Load a Calendaring OSID Provider.");
        addCommand("load_orchestration", new String[] {"laodo", "lo"}, "cmdLoadOrchestration", "Load an OrchestrationOSID Provider.");
        addCommand("status", new String[] {}, "cmdStatus", "Show status information.");
        addCommand("list_events", new String[] {"le", "ls"}, "cmdListEvents", "List events.");
        addCommand("print_events", new String[] {"pe", "p", "show"}, "cmdShowEvents", "Print an event.");
        addCommand("query_events", new String[] {"qe"}, "cmdQueryEvents", "Query events.");
        addCommand("list_calendars", new String[] {"lc"}, "cmdListCalendars", "List calendars.");
        addCommand("goto_calendar", new String[] {"go", "gc", "g"}, "cmdSelectCalendar", "Go to specified calendar.");
        addCommand("quit", new String[] {"exit", "q"}, "cmdQuit", "Quit.");

        try {            
            this.runtime = new BootLoader().getRuntimeManager("tomcal");
        } catch (OsidException oe) {
            throw new RuntimeException(oe);
        }

        updateCommandStatus();
        return;
    }


    private void updateCommandStatus() {
        if ((this.calManager == null) || !this.calManager.supportsVisibleFederation()) {
            findCommand("goto_calendar").unavailable();
        } else {
            findCommand("goto_calendar").available();
        }

        if ((this.calManager == null) || !this.calManager.supportsEventLookup()) {
            findCommand("list_events").unavailable();
            findCommand("print_events").unavailable();
        } else {
            findCommand("list_events").available();
            findCommand("print_events").available();
        }

        if ((this.calManager == null) || !this.calManager.supportsCalendarLookup()) {
            findCommand("list_calendars").unavailable();
        } else {
            findCommand("list_calendars").available();
        }

        return;
    }


    public void cmdLoadOsidProvider(String[] args) {
        if (args.length != 2) {
            cmdLoadOsidProviderHelp(args);
            return;
        }
        
        if (this.calManager != null) {
            this.calManager.close();
            this.calManager = null;
         }
        
        try {
            this.runtime = new BootLoader().getRuntimeManager("tomcal");
            this.calManager = (CalendaringManager) runtime.getManager(OSID.CALENDARING, args[1],
                                                                      OsidVersions.V3_0_0.getVersion());
        } catch (OsidException oe) {
            throw new RuntimeException("Cannot load OSID Calendaring Provider", oe);
        }
        
        updateCommandStatus();
        return;
    }

    
    protected void cmdLoadOsidProviderHelp(String[] args) {
        print(args[0]);
        println(" <implementation>");
        return;
    }


    public void cmdLoadOrchestration(String[] args) {
        if (args.length != 2) {
            cmdLoadOrchestrationHelp(args);
            return;
        }
        
        if (this.calManager != null) {
            this.calManager.close();
            this.calManager = null;
         }

        if (this.ontologyManager != null) {
            this.ontologyManager.close();
            this.ontologyManager = null;
         }
        
        try {
            this.runtime = new BootLoader().getRuntimeManager("tomcal");
            OrchestrationManager orch = (OrchestrationManager) runtime.getManager(OSID.ORCHESTRATION, args[1],
                                                                                  OsidVersions.V3_0_0.getVersion());
            
            if (!orch.supportsCalendaringProvider()) {
                throw new RuntimeException("Calendaribg OSID not supported (and this is a Calendaring OSID Consumer");
            }

            this.calManager = orch.getCalendaringManager();
            
            if (orch.supportsOntologyProvider()) {
                this.ontologyManager = orch.getOntologyManager();
            }            
        } catch (OsidException oe) {
            throw new RuntimeException("Cannot load OSID Calendaring Provider", oe);
        }
        
        updateCommandStatus();
        return;
    }

    
    protected void cmdLoadOrchestrationHelp(String[] args) {
        print(args[0]);
        println(" <implementation>");
        return;
    }


    public void cmdStatus(String[] args) {
        if (args.length > 2) {
            cmdStatusHelp(args);
            return;
        }

        if ((args.length == 2) && !args[1].equals("-v")) {
            cmdStatusHelp(args);
            return;
        }

        if (this.calManager == null) {
            println("No current OSID Provider.");
            return;
        }

        print("Using ");
        print(this.calManager.getDisplayName());
        print(" version ");
        println(this.calManager.getVersion());
        println(this.calManager.getDescription());
        
        if (args.length == 2) {
            println();
            for (Method method : CalendaringManager.class.getMethods()) {
                if ((method.getParameterTypes().length == 0) &&
                    method.getReturnType().getName().equals("boolean") &&
                    method.getName().startsWith("supports")) {

                    int ll = print(method.getName());
                    while (ll < 50) {
                        ll += print(".");
                    }

                    try {
                        if ((boolean) method.invoke(this.calManager)) {
                            println("yes");
                        } else {
                            println("no");
                        }
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        println("dunno");
                    }
                }
            }
        }

        return;
    }


    protected void cmdStatusHelp(String[] args) {
        print(args[0]);
        println(" [-v]");
        return;
    }


    public void cmdListEvents(String[] args) {
        if (args.length != 1) {
            cmdListEventsHelp(args);
            return;
        }

        if (this.calManager == null) {
            println("No current OSID Provider.");
            return;
        }

        org.osid.calendaring.EventLookupSession session = null;

        try {
            if (this.calendar == null) {
                session = this.calManager.getEventLookupSession();
                print("Calendar: default");            
            } else {
                session = this.calManager.getEventLookupSessionForCalendar(this.calendar.getId());
                print("Calendar: ");
                print(calendar.getDisplayName());
            }

            print("\t\tAgent: ");
            try {
                if (session.getEffectiveAgentId().equals(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent().getId())) {
                    print("anonymous user");
            } else {
                    print(session.getEffectiveAgent().getDisplayName());
                }
            } catch (org.osid.OsidException oe) {
                print("<cannot get agent>");
            }
            
            print("                 ");
            println(session.getDate());
            println();
            
            try (EventList events = session.getEvents()) {
                while (events.hasNext()) {
                    Event event = events.getNextEvent();
                    print(event.getId());
                    print("\t");
                    print(event.getStartDate());
                    print("\t");
                    println(event.getDisplayName());
                }
            }
        } catch (org.osid.OsidException oe) {
            oe.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return;
    }


    protected void cmdListEventsHelp(String[] args) {
        println(args[0]);
        return;
    }


    public void cmdShowEvents(String[] args) {
        if (this.calManager == null) {
            println("No current OSID Provider.");
            return;
        }

        org.osid.calendaring.EventLookupSession session = null;
        EventList events = null;

        try {
            if (this.calendar == null) {
                session = this.calManager.getEventLookupSession();
            } else {
                session = this.calManager.getEventLookupSessionForCalendar(this.calendar.getId());
            }

            if (args.length == 1) {            
                events = session.getEvents();
            } else {
                MutableIdList eventIds = new MutableIdList();
                for (int i = 1; i < args.length; i++) {
                    eventIds.addId(BasicId.valueOf(args[i]));
                }
                eventIds.eol();
                events = session.getEventsByIds(eventIds);
            }

            boolean first = true;
            while (events.hasNext()) {
                Event event = events.getNextEvent();

                if (!first) {
                    println();
                } else {
                    first = false;
                }

                println("id: \t\t\t" + event.getId());
                println("name: \t\t\t" + event.getDisplayName());
                println("description: \t\t" + event.getDescription());
                println("start time: \t\t" + event.getStartDate());            
                println("end time: \t\t" + event.getEndDate());
                println("location: \t\t" + event.getLocationDescription());
                print("sponsors: \t\t");
                ResourceList resources = event.getSponsors();
                
                boolean firstSponsor = true;
                while (resources.hasNext()) {
                    Resource resource = resources.getNextResource();
                    if (!firstSponsor) {
                        print("\t\t");
                    } else {
                        firstSponsor = false;
                    }

                    println(resource.getDisplayName());
                }

                if (this.ontologyManager != null) {
                    print("subjects: \t\t");
                    try (RelevancyList relevancies = this.ontologyManager.getRelevancyLookupSession().getRelevanciesForMappedId(event.getId())) {
                        boolean firstSubject = true;
                        while (relevancies.hasNext()) {
                            if (!firstSubject) {
                                print("\t\t");
                            } else {
                                firstSubject = false;
                            }
                            
                            println(relevancies.getNextRelevancy().getSubject().getDisplayName());
                        }
                    }
                }                    
            }
        } catch (org.osid.OsidException oe) {
            oe.printStackTrace();
        } finally {
            if (events != null) {
                events.close();
            }

            if (session != null) {
                session.close();
            }
        }

        return;
    }


    protected void cmdShowEventsHelp(String[] args) {
        print(args[0]);
        println(" [<eventId1> <eventId2> ...]");
        return;
    }


    public void cmdQueryEvents(String[] args) {
        if (args.length == 1) {
            cmdListEventsHelp(args);
            return;
        }

        if (this.calManager == null) {
            println("No current OSID Provider.");
            return;
        }

        org.osid.calendaring.EventQuerySession session = null;

        try {
            if (this.calendar == null) {
                session = this.calManager.getEventQuerySession();
                print("Calendar: default");            
            } else {
                session = this.calManager.getEventQuerySessionForCalendar(this.calendar.getId());
                print("Calendar: ");
                print(calendar.getDisplayName());
            }

            print("\t\tAgent: ");
            try {
                if (session.getEffectiveAgentId().equals(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent().getId())) {
                    print("anonymous user");
            } else {
                    print(session.getEffectiveAgent().getDisplayName());
                }
            } catch (org.osid.OsidException oe) {
                print("<cannot get agent>");
            }
            
            print("                 ");
            println(session.getDate());
            println();
            
            EventQuery query = session.getEventQuery();
            for (int i = 1; i < args.length; i++) {
                query.matchKeyword(args[i], StringMatchTypes.WORDIGNORECASE.getType(), true);
            }

            try (EventList events = session.getEventsByQuery(query)) {
                while (events.hasNext()) {
                    Event event = events.getNextEvent();
                    print(event.getId());
                    print("\t");
                    print(event.getStartDate());
                    print("\t");
                    println(event.getDisplayName());
                }
            }
        } catch (org.osid.OsidException oe) {
            oe.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return;
    }


    protected void cmdQueryEventsHelp(String[] args) {
        print(args[0]);
        println(" <query> <query>...");
        return;
    }


    public void cmdListCalendars(String[] args) {
        if (args.length != 1) {
            cmdListCalendarsHelp(args);
            return;
        }

        if (this.calManager == null) {
            println("No current OSID Provider.");
            return;
        }

        if (this.calManager.supportsCalendarHierarchy()) {
            try (CalendarHierarchySession session = this.calManager.getCalendarHierarchySession()) {
                print("Agent: ");
                try {
                    if (session.getEffectiveAgentId().equals(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent().getId())) {
                        print("anonymous user");
                    } else {
                        print(session.getEffectiveAgent().getDisplayName());
                    }
                } catch (org.osid.OsidException oe) {
                    print("<cannot get agent>");
                }
                
                print("                            ");
                println(session.getDate());
                println();
                
                try (CalendarList roots = session.getRootCalendars()) {
                    while (roots.hasNext()) {
                        printNode(session.getCalendarNodes(roots.getNextCalendar().getId(), 0, 9999, false), 0);
                    }
                } catch (org.osid.OsidException oe) {
                    oe.printStackTrace();
                }
            } catch (org.osid.OperationFailedException oe) {
                oe.printStackTrace();
                return;
            }
        } else {
            try (CalendarLookupSession session = this.calManager.getCalendarLookupSession()) {
                print("Agent: ");
                try {
                    if (session.getEffectiveAgentId().equals(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent().getId())) {
                        print("anonymous user");
                    } else {
                        print(session.getEffectiveAgent().getDisplayName());
                    }
                } catch (org.osid.OsidException oe) {
                    print("<cannot get agent>");
                }
                
                print("                            ");
                println(session.getDate());
                println();
                
                try (CalendarList calendars = session.getCalendars()) {
                    while (calendars.hasNext()) {
                        Calendar calendar = calendars.getNextCalendar();
                        print(calendar.getId());
                        print("\t");
                        println(calendar.getDisplayName());
                    }
                }
            } catch (org.osid.OsidException oe) {
                oe.printStackTrace();
            }
        }
        return;
    }

            
    private void printNode(CalendarNode node, int level) {
        int ll = 0;
        for (int i = 0; i < level; i++) {
            print (">> ");
            ll += 3;
        }

        ll += print(node.getCalendar().getId());
        for (int i = ll; i < 50; i++) {
            print (" ");
        }

        println(node.getCalendar().getDisplayName());
                  
        try (CalendarNodeList nodes = node.getChildCalendarNodes()) {
            while (nodes.hasNext()) {
                try {
                    printNode(nodes.getNextCalendarNode(), level + 1);
                } catch (OperationFailedException ofe) {
                    ofe.printStackTrace();
                }
            }
        }

        return;
    }


    protected void cmdListCalendarsHelp(String[] args) {
        println(args[0]);
        return;
    }


    public void cmdSelectCalendar(String[] args) {
        if (args.length == 1) {
            this.calendar = null;
            return;
        }

        if (args.length > 2) {
            cmdSelectCalendarHelp(args);
            return;
        }

        if (this.calManager == null) {
            println("No current OSID Provider.");
            return;
        }

        try (org.osid.calendaring.CalendarLookupSession session = this.calManager.getCalendarLookupSession()) {
            this.calendar = session.getCalendar(BasicId.valueOf(args[1]));
        } catch (org.osid.OsidException oe) {
            oe.printStackTrace();
        }

        return;
    }


    protected void cmdSelectCalendarHelp(String[] args) {
        print(args[0]);
        println(" [<calendar Id>]");
        return;
    }

               
    public static void main(String... args) {
        TomCal tc = new TomCal();
        tc.run();
    }
}
