
package tom.e07;

import org.osid.binding.java.annotation.OSID;
import java.util.*;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  A John Hancock weather ontology.
 */

public final class OntologyManager 
    extends net.okapia.osid.jamocha.ontology.spi.AbstractOntologyManager
    implements org.osid.ontology.OntologyManager,
               org.osid.ontology.OntologyProxyManager {

    private static final String ID           = "urn:osid:tom.net:identifiers:providers:ontology:e07";
    private static final String DISPLAY_NAME = "Weather Beacon";
    private static final String DESCRIPTION  = "An ontological provider of everything you need to know about weather.";
    private static final String VERSION      = "0.0.1";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901210Z";
    private static final org.osid.ontology.Ontology ONTOLOGY = new Ontology();

    private final Collection<org.osid.ontology.Subject> subjectDB = new ArrayList<>();
    private final Collection<org.osid.ontology.Relevancy> relevancyDB = new ArrayList<>();


    public OntologyManager() {
        super(new OntologyProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidRuntime </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        
        populateSubjects();
        populateRelevancies();

        return;
    }
   
    
    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (true);
    }


    /**
     *  Tests if an subject lookup service is supported. An subject lookup service 
     *  defines methods to access subjects. 
     *
     *  @return true if subject lookup is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsSubjectLookup() {
        return (true);
    }

    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service. 
     *
     *  @return an <code> SubjectLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSession()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.core.ontology.InvariantMapSubjectLookupSession(ONTOLOGY, this.subjectDB));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return an <code> SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        if (!ONTOLOGY.getId().equals(ontologyId)) {
            throw new org.osid.NotFoundException(ontologyId + " not found");
        }

        return (getSubjectLookupSession());
    }


    /**
     *  Tests if an relevancy lookup service is supported. An
     *  relevancy lookup service defines methods to access relevancys.
     *
     *  @return true if relevancy lookup is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsRelevancyLookup() {
        return (true);
    }

    /**
     *  Gets the <code> OsidSession </code> associated with the
     *  relevancy lookup service.
     *
     *  @return an <code> RelevancyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRelevancyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSession()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.core.ontology.InvariantMapRelevancyLookupSession(ONTOLOGY, this.relevancyDB));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy lookup 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return an <code> RelevancyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsRelevancyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        if (!ONTOLOGY.getId().equals(ontologyId)) {
            throw new org.osid.NotFoundException(ontologyId + " not found");
        }

        return (getRelevancyLookupSession());
    }


    /**
     *  Tests if a ontology lookup service is supported. 
     *
     *  @return <code> true </code> if ontology lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyLookup() {
        return (true);
    }

    
    /**
     *  Gets the OsidSession associated with the ontology lookup service. 
     *
     *  @return a <code> OntologyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyLookupSession getOntologyLookupSession()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.core.ontology.InvariantMapOntologyLookupSession(ONTOLOGY));
    }


    /*
     * Populate the database with Subjects from our data source.
     */
    
    private void populateSubjects() {
        for (WeatherBeacon signal : WeatherBeacon.values()) {
            this.subjectDB.add(new Subject(signal));
        }

        return;
    }


    /*
     * Populate the database with Relevancies from our data source.
     */
    
    private static final String[][] relevancies = {
        {"weather beacon:SOLID_BLUE@John Hancock", "game:G1@MLB"},
        {"weather beacon:SOLID_BLUE@John Hancock", "game:G2@MLB"},
        {"weather beacon:BLINKING_BLUE@John Hancock", "game:G3@MLB"},
        {"weather beacon:SOLID_BLUE@John Hancock", "game:G4@MLB"},
        {"weather beacon:BLINKING_BLUE@John Hancock", "game:G5@MLB"},
        {"weather beacon:SOLID_RED@John Hancock", "game:G9@MLB"},
        {"weather beacon:BLINKING_RED@John Hancock", "game:G10@MLB"}
    };


    private void populateRelevancies() {
        for (String[] relevancy : relevancies) {
            this.relevancyDB.add(new Relevancy(this, net.okapia.osid.primordium.id.BasicId.valueOf(relevancy[0]), 
                                               net.okapia.osid.primordium.id.BasicId.valueOf(relevancy[1])));
        }

        return;
    }


    /*
     * See e01.
     */

    static class OntologyProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        OntologyProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new tom.TomProvider());

            return;
        }
    }
}

