package tom.e07;

public enum WeatherBeacon {
    SOLID_BLUE ("Solid Blue", "Clear skies."),
    BLINKING_BLUE ("Blinking Blue", "Clouds due."),
    SOLID_RED ("Solid Red", "Rain ahead."),
    BLINKING_RED ("Blinking Red", "Snow instead (or the Sox game in canceled).");
    
    private final String name;
    private final String description;


    WeatherBeacon(String name, String description) {
        this.name        = name;
        this.description = description;

        return;
    }

        
    public String getName() {
        return (this.name);
    }


    public String getDescription() {
        return (this.description);
    }
}
                    
        
