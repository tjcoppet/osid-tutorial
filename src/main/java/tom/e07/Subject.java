package tom.e07;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


public final class Subject
    extends net.okapia.osid.jamocha.ontology.subject.spi.AbstractSubject
    implements org.osid.ontology.Subject {

    private static final org.osid.type.Type GENUS_TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:tom.net:types:genera:ontology:Subject:Hancock");


    public Subject(WeatherBeacon signal) {
        current(); // it is always up to date

        setId(new net.okapia.osid.primordium.id.BasicId("John Hancock", "weather beacon", signal.name()));
        setDisplayName(text(signal.getName()));
        setDescription(text(signal.getDescription()));
        setGenusType(GENUS_TYPE);
        
        return;
    }
}

