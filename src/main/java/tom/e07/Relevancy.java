package tom.e07;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


public final class Relevancy
    extends net.okapia.osid.jamocha.ontology.relevancy.spi.AbstractRelevancy
    implements org.osid.ontology.Relevancy {

    private static final org.osid.type.Type GENUS_TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:tom.net:types:genera:ontology:Relevancy");
    private static int id = 1;

    private final org.osid.id.Id subjectId;
    private final OntologyManager mgr;


    Relevancy(OntologyManager mgr, org.osid.id.Id subjectId, org.osid.id.Id referenceId) {
        setId(new net.okapia.osid.primordium.id.BasicId("tom.net", "relevancy", String.valueOf(++id)));
        setDisplayName(text("weather"));
        setDescription(text("What Mr. Hancock says."));
        setGenusType(GENUS_TYPE);
        setMappedId(referenceId);

        this.subjectId = subjectId;
        this.mgr = mgr;

        return;
    }


    /**
     *  Gets the <code> Subject. </code>
     *
     *  @return the subject
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.ontology.Subject getSubject()
        throws org.osid.OperationFailedException {

        try {
            return (this.mgr.getSubjectLookupSession().getSubject(this.subjectId));
        } catch (org.osid.NotFoundException | org.osid.PermissionDeniedException e) {
            throw new org.osid.OperationFailedException(e);
        }
    }
}

