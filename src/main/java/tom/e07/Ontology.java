package tom.e07;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  Defines a Ontology OsidCatalog.
 */

public final class Ontology
    extends net.okapia.osid.jamocha.ontology.ontology.spi.AbstractOntology
    implements org.osid.ontology.Ontology {


    public Ontology() {
        current();

        setId(new net.okapia.osid.primordium.id.BasicId("tom.net", "osid.ontology.Ontology", "Weather"));
        setDisplayName(text("Weather Beacon"));
        setDescription(text("Weather beacon signals from the John Hancock building."));

        return;
    }
}



