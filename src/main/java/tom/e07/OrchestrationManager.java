
package tom.e07;

import org.osid.binding.java.annotation.OSID;
import java.util.*;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


public final class OrchestrationManager 
    extends net.okapia.osid.jamocha.orchestration.spi.AbstractOrchestrationManager
    implements org.osid.orchestration.OrchestrationManager {

    private static final String ID           = "urn:osid:tom.net:identifiers:providers:orchestration:e07";
    private static final String DISPLAY_NAME = "Weather Beacon";
    private static final String DESCRIPTION  = "An ontological provider of everything you need to know about weather.";
    private static final String VERSION      = "0.0.1";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901210Z";


    public OrchestrationManager() {
        super(new OrchestrationProvider());
        return;
    }


    /**
     *  Tests if a calendaring provider is supported. 
     *
     *  @return <code> true </code> if a calendaring provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringProvider() {
        return (true);
    }


    /**
     *  Gets the manager associated with the Calendaring service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringProvider() </code> is <code> false </code> 
     *  @compliance optional This method must be implemented if <code> 
     *              supportsCalendaringProvider() </code> is <code> true. 
     *              </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendaringManager getCalendaringManager()
        throws org.osid.OperationFailedException {
        
        try {
            return ((org.osid.calendaring.CalendaringManager) getOsidManager(org.osid.OSID.CALENDARING,
                                                                             "tom.e03.CalendaringManager"));
        } catch (org.osid.ConfigurationErrorException ce) {
            throw new org.osid.OperationFailedException (ce);
        }
    }


    /**
     *  Tests if an ontology provider is supported. 
     *
     *  @return <code> true </code> if an ontology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyProvider() {
        return (true);
    }


    /**
     *  Gets the manager associated with the Ontology service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyManager getOntologyManager()
        throws org.osid.OperationFailedException {

        try {
            return ((org.osid.ontology.OntologyManager) getOsidManager(org.osid.OSID.ONTOLOGY, 
                                                                       "tom.e07.OntologyManager"));
        } catch (org.osid.ConfigurationErrorException ce) {
            throw new org.osid.OperationFailedException (ce);
        }
    }



    static class OrchestrationProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        OrchestrationProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new tom.TomProvider());

            return;
        }
    }
}

