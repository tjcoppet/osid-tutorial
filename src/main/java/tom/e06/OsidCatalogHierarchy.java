
package tom.e06;

/*
 * Wanted a no fuss way to convert a Calendar to a Hierarchy.
 */

public class OsidCatalogHierarchy
    implements java.lang.reflect.InvocationHandler {

    private final org.osid.OsidCatalog catalog;


    private OsidCatalogHierarchy(org.osid.OsidCatalog catalog) {
        this.catalog = catalog;
        return;
    }
    
    
    public static org.osid.hierarchy.Hierarchy newInstance(org.osid.OsidCatalog catalog) {
        
        return ((org.osid.hierarchy.Hierarchy) java.lang.reflect.Proxy.newProxyInstance(catalog.getClass().getClassLoader(), 
                                                                                        new Class[] {org.osid.hierarchy.Hierarchy.class},
                                                                                        new OsidCatalogHierarchy(catalog)));
    }


    public Object invoke(Object proxy, java.lang.reflect.Method m, Object[] args) 
        throws Throwable {
        
        try {           
            return (m.invoke(this.catalog, args));
        } catch (java.lang.reflect.InvocationTargetException e) {
            throw e.getTargetException();
        }
    }
}
    