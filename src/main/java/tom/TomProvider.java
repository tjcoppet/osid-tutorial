package tom;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.primordium.locale.text.eng.us.Html;


/**
 *  An osid.resource.Resource to express my tutorial persona.
 */

public final class TomProvider 
    extends net.okapia.osid.provider.spi.AbstractProvider
    implements org.osid.resource.Resource {

    private static final org.osid.id.Id ID = BasicId.valueOf("provider:me@tom.net");

    private static final org.osid.locale.DisplayText DISPLAY_NAME = Plain.valueOf("TomSoft");
    private static final org.osid.locale.DisplayText DESCRIPTION  = Html.valueOf("<p>TomSoft is a new venture in developing code examples for OSID tutorials. I hope to take this public someday.</p>");

    private static final org.osid.locale.DisplayText LICENSE = Html.valueOf("<p>This implementation (\"Work\") and the information contained herein is provided on an \"AS IS\" basis. Tom Coppeto, and THE AUTHORS DISCALIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OF IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS IN THE WORK.</p><p>Permission to use, copy, modify, adapt and distribute this Work, for any purpose, without fee or royalty is hereby granted, provided that you include the above copyright notice and the terms of this license on ALL copies of the Work of portions thereof.</p><p>The export of software employing encryption technology may require a specific license from the United States Government. It is the responsibility of any person or organization contemplating export to obtain such a license before exporting this Work.</p>");


    public TomProvider() {
        setId(ID);
        setDisplayName(DISPLAY_NAME);
        setDescription(DESCRIPTION);
        setLicense(LICENSE);

        return;
    }


    /**
     *  This information doesn't change. 
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean isCurrent() {
        return (true);
    }
}




    


