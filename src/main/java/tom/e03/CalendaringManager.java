
package tom.e03;

import org.osid.binding.java.annotation.OSID;
import java.util.*;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  This version supports visible federation.
 *
 *  Also got rid of our custom EventLookupSession.
 */

public final class CalendaringManager 
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCalendaringManager
    implements org.osid.calendaring.CalendaringManager,
               org.osid.calendaring.CalendaringProxyManager {

    private static final String ID           = "urn:osid:tom.net:identifiers:providers:calendaring:e03";
    private static final String DISPLAY_NAME = "TomCal";
    private static final String DESCRIPTION  = "My third OSID Calendaring Provider. This will bring it into balance.";
    private static final String VERSION      = "0.0.3";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901236Z";
    private static final org.osid.calendaring.Calendar CALENDAR = new MLBCalendar();

    /* next generation rdbms */
    private final Collection<org.osid.calendaring.Event> eventDB = new ArrayList<>();


    public CalendaringManager() {
        super(new CalendaringProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidRuntime </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        /* stashes the runtime so I can take a few short cuts later */
        super.initialize(runtime);

        populateEvents();

        return;
    }
   

    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (true);
    }


    /**
     *  Tests if an event lookup service is supported. An event lookup service 
     *  defines methods to access events. 
     *
     *  @return true if event lookup is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsEventLookup() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service. 
     *
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSession()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.core.calendaring.InvariantMapEventLookupSession(CALENDAR, this.eventDB));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        /*
         * We support a single Calendar.
         */

        if (!CALENDAR.getId().equals(calendarId)) {
            throw new org.osid.NotFoundException(calendarId + " not found");
        }

        return (getEventLookupSession());
    }


    /**
     *  Tests if a calendar lookup service is supported. 
     *
     *  @return <code> true </code> if calendar lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarLookup() {
        return (true);
    }

    
    /**
     *  Gets the OsidSession associated with the calendar lookup service. 
     *
     *  @return a <code> CalendarLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarLookupSession getCalendarLookupSession()
        throws org.osid.OperationFailedException {

        /*
         * Only one Calendar exists, and it never changes, so I can
         * easily nail it up.
         */

        return (new net.okapia.osid.jamocha.core.calendaring.InvariantMapCalendarLookupSession(CALENDAR));
    }


    /*
     * Populate the database with Events from our data source.
     */
    
    private void populateEvents() {
        for (GameData game : GameData.values()) {
            org.osid.calendaring.Event event = new MLBGame(game); 
            net.okapia.osid.jamocha.builder.validator.calendaring.event.EventValidator.validateEvent(event);
            this.eventDB.add(event);
        }

        return;
    }


    /*
     * See e01.
     */

    static class CalendaringProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        CalendaringProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new tom.TomProvider());

            return;
        }
    }
}

