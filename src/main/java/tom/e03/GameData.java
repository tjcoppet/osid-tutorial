package tom.e03;

public enum GameData {

    G1 ("2017-08-22T23:10:00Z", "2017-08-23T02:10:00Z", "Red Sox vs. Indians", "Progressive Field", "Cleveland, OH, US"), 
    G2 ("2017-08-23T23:10:00Z", "2017-08-24T02:10:00Z", "Red Sox vs. Indians", "Progressive Field", "Cleveland, OH, US"), 
    G3 ("2017-08-24T23:10:00Z", "2017-08-25T02:10:00Z", "Red Sox vs. Indians", "Progressive Field", "Cleveland, OH, US"), 
    G4 ("2017-08-25T23:10:00Z", "2017-08-26T02:10:00Z", "Orioles vs. Red Sox", "Fenway Park", "Boston, MA, US"),
    G5 ("2017-08-26T20:05:00Z", "2017-08-27T00:05:00Z", "Orioles vs. Red Sox", "Fenway Park", "Boston, MA, US"),
    G6 ("2017-08-27T17:35:00Z", "2017-08-27T21:35:00Z", "Orioles vs. Red Sox", "Fenway Park", "Boston, MA, US"),
    G7 ("2017-08-28T17:35:00Z", "2017-08-28T21:35:00Z", "Red Sox vs. Blue Jays", "Rogers Centre", "Toronto, ON, CA"),
    G8 ("2017-08-29T23:07:00Z", "2017-08-30T02:07:00Z", "Red Sox vs. Blue Jays", "Rogers Centre", "Toronto, ON, CA"),
    G9 ("2017-08-30T23:07:00Z", "2017-08-31T02:07:00Z", "Red Sox vs. Blue Jays", "Rogers Centre", "Toronto, ON, CA"),
    G10 ("2017-08-31T23:05:00Z", "2017-09-01T02:05:00Z", "Red Sox vs. Yankees", "Yankee Stadium", "New York, NY, US");
    
    private final String start;
    private final String end;
    private final String name;
    private final String stadium;
    private final String city;


    GameData(String start, String end, String name, String stadium, String city) {
        this.start       = start;
        this.end         = end;
        this.name        = name;
        this.stadium     = stadium;
        this.city        = city;;

        return;
    }

        
    public String getStart() {
        return (this.start);
    }

    
    public String getEnd() {
        return (this.end);
    }


    public String getName() {
        return (this.name);
    }


    public String getStadium() {
        return (this.stadium);
    }


    public String getCity() {
        return (this.city);
    }
}
                    
        
