package tom.e03;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 * This Event represents a major league baseball game which I will use
 * to integrate with GameData. The GameData is sparse, so I will fill
 * in some OSID methods here. These methods are providing context for
 * anyone looking down to make some sense out of this in a potentially
 * large soup of Events.
 */

public final class MLBGame
    extends net.okapia.osid.jamocha.calendaring.event.spi.AbstractEvent
    implements org.osid.calendaring.Event {

    private static final org.osid.type.Type GENUS_TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:tom.net:types:genera:calendaring:Event:MLB");

    /*
     * I'm going to nail up a Resource to be the sponsor of these Events.
     */

    private static final org.osid.resource.Resource MLB = new net.okapia.osid.jamocha.builder.resource.resource.ResourceBuilder()
        .id(net.okapia.osid.primordium.id.BasicId.valueOf("resource:mlb@tom.net"))
        .displayName(text("MLB"))
        .description(text("Major League Baseball"))
        .genus(net.okapia.osid.primordium.type.BasicType.valueOf("resource:Organization@tom.net"))
        .build();


    public MLBGame(GameData game) {
        current(); // it is always up to date

        setId(new net.okapia.osid.primordium.id.BasicId("MLB", "game", game.name()));
        setDisplayName(text(game.getName()));
        setDescription(text(game.getName() + " regular season game."));
        setGenusType(GENUS_TYPE);
        
        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.valueOf(game.getStart()));
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.valueOf(game.getEnd()));
        setDuration(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.valueOf("3 hours"));

        setLocationDescription(text(game.getStadium() + " in " + game.getCity() + "."));
        addSponsor(MLB);

        return;
    }
}

