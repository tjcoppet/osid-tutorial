
package tom.e04;

import org.osid.binding.java.annotation.OSID;
import net.okapia.osid.primordium.types.search.StringMatchTypes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
 * This is an implementation of EventQuery but is also incorporated a
 * filtering tool.
 */

public final class EventQueryFilter
    extends net.okapia.osid.jamocha.calendaring.event.spi.AbstractEventQuery
    implements org.osid.calendaring.EventQuery,
               net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilter {

    private boolean matchAny = true;
    private java.util.Collection<StringTerm> terms = new java.util.LinkedHashSet<>();


    EventQueryFilter() {
        addStringMatchType(StringMatchTypes.EXACT.getType());
        addStringMatchType(StringMatchTypes.IGNORECASE.getType());
        addStringMatchType(StringMatchTypes.WORD.getType());
        addStringMatchType(StringMatchTypes.WORDIGNORECASE.getType());
        addStringMatchType(StringMatchTypes.REGEX.getType());
        addStringMatchType(StringMatchTypes.WILDCARD.getType());

        return;
    }


    /**
     *  Adds a keyword to match. Multiple keywords can be added to perform a 
     *  boolean <code> OR </code> among them. A keyword may be applied to any 
     *  of the elements defined in this object such as the display name, 
     *  description or any method defined in an interface implemented by this 
     *  object. 
     *
     *  @param  keyword keyword to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> keyword </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchKeyword(String keyword, org.osid.type.Type stringMatchType, boolean match) {
        if (!supportsStringMatchType(stringMatchType)) {
            throw new org.osid.UnsupportedException(stringMatchType + " is not supported");
        }

        this.terms.add(new StringTerm(keyword, stringMatchType, match));
        return;
    }


    /**
     *  Clears all keyword terms. 
     */

    @OSID @Override
    public void clearKeywordTerms() {
        this.terms.clear();
        return;
    }


    /**
     *  Matches any object. 
     *
     *  @param match <code> true </code> to match any object, <code>
     *         false </code> to match no objectsx
     */

    @OSID @Override
    public void matchAny(boolean match) {
        this.matchAny = match;
        return;
    }


    /**
     *  Clears the match any terms. 
     */

    @OSID @Override
    public void clearAnyTerms() {
        this.matchAny = false;
        return;
    }


    /**
     *  Event Filter.
     *
     *  @param event the event to filter
     *  @return <code>true</code> if the event passes the filter,
     *          <code>false</code> if the event should be filtered
     */

    @Override
    public boolean pass(org.osid.calendaring.Event event) {
        if (matchesAny() && matchesKeyword(event) && matchesDisplayName(event) && matchesDescription(event)) {
            return (true);
        } else {
            return (false);
        }
    }

    
    private boolean matchesAny() {
        return (this.matchAny);
    }


    private boolean matchesKeyword(org.osid.calendaring.Event event) {
        return (matchesDisplayName(event) || matchesDescription(event));
    }


    private boolean matchesDisplayName(org.osid.calendaring.Event event) {
        for (StringTerm term : this.terms) {
            if (!matchesString(term, event.getDisplayName().getText())) {
                return (false);
            }
        }

        return (true);
    }


    private boolean matchesDescription(org.osid.calendaring.Event event) {
        for (StringTerm term : this.terms) {
            if (!matchesString(term, event.getDisplayName().getText())) {
                return (false);
            }
        }

        return (true);
    }


    private boolean matchesString(StringTerm term, String text) {        
        boolean match = false;

        if (term.getMatchType().equals(StringMatchTypes.EXACT.getType())) {
            if (term.getString().equals(text)) {
                return (term.getMatch());
            } else {
                return (!term.getMatch());
            }
        }

        if (term.getMatchType().equals(StringMatchTypes.IGNORECASE.getType())) {   
            if (term.getString().equalsIgnoreCase(text)) {
                return (term.getMatch());
            } else {
                return (!term.getMatch());
            }
        }

        if (term.getMatchType().equals(StringMatchTypes.WORD.getType())) {   
            String regex = "\\b" + term.getString() + "\\b";
            
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);

            if (matcher.find()) {
                return (term.getMatch());
            } else {
                return (!term.getMatch());
            }
        }

        if (term.getMatchType().equals(StringMatchTypes.WORDIGNORECASE.getType())) {   
            String regex = "\\b" + term.getString() + "\\b";
            
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(text);

            if (matcher.find()) {
                return (term.getMatch());
            } else {
                return (!term.getMatch());
            }
        }


        if (term.getMatchType().equals(StringMatchTypes.REGEX.getType())) {   
            String regex = term.getString();
            
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(text);

            if (matcher.find()) {
                return (term.getMatch());
            } else {
                return (!term.getMatch());
            }
        }

        return (false);
    }


    static class StringTerm {
        private final org.osid.type.Type matchType;
        private final String str;
        private final boolean match;

        
        StringTerm(String str, org.osid.type.Type matchType, boolean match) {
            this.matchType = matchType;
            this.str = str;
            this.match = match;

            return;
        }

        
        String getString() {
            return (this.str);
        }


        org.osid.type.Type getMatchType() {
            return (this.matchType);
        }

        
        boolean getMatch() {
            return (this.match);
        }
    }                  
}