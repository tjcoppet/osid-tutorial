
package tom.e04;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/*
 * Uses an EventLookupSession to perform queries.
 *
 * I copied the java interface, mapped them to th EventLookupSession
 * and accounted for what was left out.
 */

public final class EventQuerySession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractEventQuerySession
    implements org.osid.calendaring.EventQuerySession {

    private final org.osid.calendaring.EventLookupSession session;


    EventQuerySession(org.osid.calendaring.EventLookupSession session) {
        this.session = session;
        
        /*
         * Not all the view line up, so we'll just set these.
         */

        session.useComparativeEventView();
        session.useAnyEffectiveEventView();
        
        return;
    }


    /**
     *  Gets the locale indicating the localization preferences in
     *  effect for this session. 
     *
     *  Uses a specific locale set, then falls back to the proxy.
     *
     *  @return the locale 
     */

    @OSID @Override
    public synchronized org.osid.locale.Locale getLocale() { 
        return (this.session.getLocale());
    }


    /**
     *  Tests if there are valid authentication credentials used by this 
     *  service. 
     *
     *  Valid credentials exist of the agent is explicitly set, or an
     *  Authentication was provided where isValid() is true, or a
     *  proxy was provided that contains an Authentication where
     *  isValid() is true.
     *
     *  @return <code> true </code> if valid authentication
     *          credentials exist, <code> false </code> otherwise
     */

    @OSID @Override
    public synchronized boolean isAuthenticated() { 
        return (this.session.isAuthenticated());
    }


    /**
     *  Gets the <code> Id </code> of the agent authenticated to this
     *  session.  
     *
     *  The agent may be explicitly set, or through Authentication
     *  provided where isValid() is true, or through proxy provided
     *  that contains an Authentication where isValid() is true.
     *
     *  @return the authenticated agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isAuthenticated()</code> is <code>false</code>
     */

    @OSID @Override
    public synchronized org.osid.id.Id getAuthenticatedAgentId() {
        return (this.session.getAuthenticatedAgentId());
    }


    /**
     *  Gets the agent authenticated to this session. 
     *
     *  The agent may be explicitly set, or through Authentication
     *  provided where isValid() is true, or through proxy provided
     *  that contains an Authentication where isValid() is true.
     *
     *  @return the authenticated agent 
     *  @throws org.osid.IllegalStateException <code> isAuthenticated() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public synchronized org.osid.authentication.Agent getAuthenticatedAgent()
        throws org.osid.OperationFailedException {
        
        return (this.session.getAuthenticatedAgent());
    }


    /**
     *  Gets the <code> Id </code> of the effective agent in use by this 
     *  session. If <code> isAuthenticated() </code> is true, then the 
     *  effective agent may be the same as the agent returned by <code> 
     *  getAuthenticatedAgent(). </code> If <code> isAuthenticated() </code> 
     *  is <code> false, </code> then the effective agent may be a default 
     *  agent used for authorization by an unknwon or anonymous user. 
     *
     *  @return the effective agent 
     */

    @OSID @Override
    public org.osid.id.Id getEffectiveAgentId() {
        return (this.session.getEffectiveAgentId());
    }


    /**
     *  Gets the effective agent in use by this session.
     *
     *  This method returns the effective agent if explicitly set,
     *  then the Proxy. If none is found and the session is
     *  authenticated, the authenticated agent is used.
     *
     *  @return the effective agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public synchronized org.osid.authentication.Agent getEffectiveAgent()
        throws org.osid.OperationFailedException {

        return (this.session.getEffectiveAgent());
    }


    /**
     *  Gets the service date which may be the current date or the
     *  effective date in which this session exists.
     *
     *  @return the service date
     */

    @OSID @Override
    public synchronized java.util.Date getDate() {
        return (this.session.getDate());
    }


    /**
     *  Gets the rate of the service clock.
     *
     *  @return the clock rate
     */

    @OSID @Override
    public synchronized java.math.BigDecimal getClockRate() {
        return (this.session.getClockRate());
    }


    /**
     *  Gets the <code> DisplayText </code> format <code> Type </code> 
     *  preference in effect for this session. 
     *
     *  @return the effective <code> DisplayText </code> format <code> Type 
     *          </code> 
     */

    @OSID @Override
    public synchronized org.osid.type.Type getFormatType() {
        return (this.session.getFormatType());
    }

    
    /**
     *  Tests for the availability of transactions. 
     *
     *  @return <code> true </code> if transaction methods are available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactions() {
        return (this.session.supportsTransactions());
    }


    /**
     *  Starts a new transaction for this sesson. Transactions are a
     *  means for an OSID to provide an all-or-nothing set of
     *  operations within a session and may be used to coordinate this
     *  service from an external transaction manager. A session
     *  supports one transaction at a time.  Starting a second
     *  transaction before the previous has been committed or aborted
     *  results in an <code> ILLEGAL_STATE </code> error.
     *
     *  @return a new transaction 
     *  @throws org.osid.IllegalStateException a transaction is already open 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException transactions not supported 
     */

    @OSID @Override
    public org.osid.transaction.Transaction startTransaction()
        throws org.osid.IllegalStateException,
               org.osid.OperationFailedException,
               org.osid.UnsupportedException {

        return (this.session.startTransaction());
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.session.close();
        return;
    }


    /**
     *  Gets the <code> Calendar </code> <code> Id </code> associated with 
     *  this session. 
     *
     *  @return the <code> Calendar Id </code> associated with this session 
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code> Calendar </code> associated with this session. 
     *
     *  @return the <code> Calendar </code> associated with this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code> Event </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchEvents() {
        return (this.session.canLookupEvents());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }


    /**
     *  A normalized view uses a single <code> Event </code> to represent a 
     *  set of recurring events. 
     */

    @OSID @Override
    public void useNormalizedEventView() {
        this.session.useNormalizedEventView();
        return;
    }


    /**
     *  A denormalized view expands recurring events into a series of <code> 
     *  Events. </code> 
     */

    @OSID @Override
    public void useDenormalizedEventView() {
        this.session.useDenormalizedEventView();
        return;
    }


    /**
     *  The returns from the search methods omit sequestered events. 
     */

    @OSID @Override
    public void useSequesteredEventView() {
        this.session.useSequesteredEventView();
        return;
    }


    /**
     *  All replies are returned including sequestered events. 
     */

    @OSID @Override
    public void useUnsequesteredEventView() {
        this.session.useUnsequesteredEventView();
        return;
    }


    /**
     *  Gets an event query. 
     *
     *  @return the event query 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        return (new EventQueryFilter());
    }


    /**
     *  Gets a list of <code> Events </code> matching the given event query. 
     *
     *  @param  eventQuery the event query 
     *  @return the returned <code> EventList </code> 
     *  @throws org.osid.NullArgumentException <code> eventQuery </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> eventQuery </code> is not 
     *          of this service 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByQuery(org.osid.calendaring.EventQuery eventQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        if (!(eventQuery instanceof EventQueryFilter)) {
            throw new org.osid.UnsupportedException("this isn't mine!");
        }

        /* unwrap it and peek inside */
        EventQueryFilter filter = (EventQueryFilter) eventQuery;
        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilterList(filter, this.session.getEvents()));
    }
}
