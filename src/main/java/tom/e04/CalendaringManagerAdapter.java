
package tom.e04;

import org.osid.binding.java.annotation.OSID;
import java.util.*;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  This adds support for an EventQuerySession on top of the previous
 *  lookup-only Calendaring OSID Provider.
 */

public final class CalendaringManagerAdapter
    extends net.okapia.osid.jamocha.adapter.calendaring.spi.AbstractAdapterCalendaringManager
    implements org.osid.calendaring.CalendaringManager {

    private static final String ID           = "urn:osid:scott.com:identifiers:providers:calendaring:e04";
    private static final String DISPLAY_NAME = "ScottCal Query Adapter";
    private static final String DESCRIPTION  = "ScottCal is the next generation calendaring system supporting more than TomCal ever did.";
    private static final String VERSION      = "0.0.1";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901237Z";

    private static final org.osid.id.Id PROVIDER_PARAMETER_ID = net.okapia.osid.primordium.id.BasicId.valueOf("configuration:provider@local");    


    public CalendaringManagerAdapter() {
        super(new CalendaringProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidRuntime </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);

        setAdapteeManager((org.osid.calendaring.CalendaringManager) getOsidManager(org.osid.OSID.CALENDARING, 
                                                                                   getConfigurationValue(PROVIDER_PARAMETER_ID).getStringValue()));
    
        return;
    }
   

    /**
     *  Tests if an event query service is supported. An event query service 
     *  defines methods to access events. 
     *
     *  @return true if event query is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsEventQuery() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service. 
     *
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySession()
        throws org.osid.OperationFailedException {

        /*
         * You should start getting the sense that the main function
         * of an OsidManager implementation is to figure out what
         * OsidSession implementations to use. It's essentially an
         * implementation switchboard.
         */

        return (new EventQuerySession(getEventLookupSession()));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (new EventQuerySession(getEventLookupSessionForCalendar(calendarId)));
    }


    /*
     * See e01.
     */

    static class CalendaringProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        CalendaringProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new ScottProvider());

            return;
        }
    }
}

