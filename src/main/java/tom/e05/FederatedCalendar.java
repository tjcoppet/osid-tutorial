package tom.e05;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  Defines a Calendar OsidCatalog.
 */

public final class FederatedCalendar
    extends net.okapia.osid.jamocha.calendaring.calendar.spi.AbstractCalendar
    implements org.osid.calendaring.Calendar {


    public FederatedCalendar() {
        current();
        setId(new net.okapia.osid.primordium.id.BasicId("tom.net", "osid.calendaring.Calendar", "federated"));
        setDisplayName(text("Root"));
        setDescription(text("The top-level Calendar of this federation. Live long, and propser."));

        return;
    }
}



