
package tom.e05;

import org.osid.binding.java.annotation.OSID;
import java.util.*;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  
 */

public final class CalendaringManagerFederator
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCalendaringManager
    implements org.osid.calendaring.CalendaringManager {

    private static final String ID           = "urn:osid:tom.net:identifiers:providers:calendaring:e05";
    private static final String DISPLAY_NAME = "Calendaring Federaator";
    private static final String DESCRIPTION  = "TomCal is back and moving into the cloud.";
    private static final String VERSION      = "0.0.1";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901238Z";

    private static final org.osid.calendaring.Calendar CALENDAR = new FederatedCalendar();
    private static final org.osid.id.Id PROVIDER_PARAMETER_ID   = net.okapia.osid.primordium.id.BasicId.valueOf("configuration:provider@local");    

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.CalendaringManager> providers = new HashMap<>();


    public CalendaringManagerFederator() {
        super(new CalendaringProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidRuntime </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        
        /* this time, I'm ready for multiple values */
        try (org.osid.configuration.ValueList values = getConfigurationValues(PROVIDER_PARAMETER_ID)) {
            while (values.hasNext()) {
                org.osid.calendaring.CalendaringManager mgr = (org.osid.calendaring.CalendaringManager) getOsidManager(org.osid.OSID.CALENDARING, 
                                                                                                                       values.getNextValue().getStringValue());
                
                /* 
                 * Notice the assumption I put on the provider Id. What if
                 * they collided? This is something I will have to deal
                 * with when the problem pops up, and I either add some
                 * salt to the key or put an adapter on top of the
                 * underlying OsidManager and change its Id (this is an
                 * issue across all applications of the federating
                 * pattern).
                 */
                
                this.providers.put(mgr.getId(), mgr);
            }
        }
    
        return;
    }
   

    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (true);
    }


    /**
     *  Tests if an event lookup service is supported. An event lookup service 
     *  defines methods to access events. 
     *
     *  @return true if event lookup is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsEventLookup() {

        /*
         * I have a choice here. I can always say yes in which case
         * the federator never returns anything. Or, as shown here,
         * this adapter turns off support if none of the underlying
         * OSID Providers support it.
         */

        for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
            if (mgr.supportsEventLookup()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service. 
     *
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSession()
        throws org.osid.OperationFailedException {

        /*
         * I can't pass this off to
         * getEventLookupSessionForCalendar(). Each underlying OSID
         * Provider has a different set of Calendars so this will rely
         * on their defaults.
         */

        net.okapia.osid.jamocha.adapter.federator.calendaring.FederatingEventLookupSession session =
            new net.okapia.osid.jamocha.adapter.federator.calendaring.FederatingEventLookupSession(CALENDAR);

        for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
            if (mgr.supportsEventLookup()) {
                session.addSession(mgr.getEventLookupSession());
            }
        }

        return (session);                
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        /*
         * The logic here gets screwy. If our federated Calendar is
         * requested, I may as well pass it back to
         * getEventLookupSession(). Otherwise, it is likely that the
         * calendar Id is only known to subset, if not just one, OSID
         * Provider.
         */

        if (CALENDAR.getId().equals(calendarId)) {
            return (getEventLookupSession());
        }

        /*
         * I can leverage the federated getCalendar() method to help me out.
         */

        org.osid.calendaring.Calendar calendar;

        /* honoring the NotFoundException */
        try {
            calendar = getCalendarLookupSession().getCalendar(calendarId);
        } catch (org.osid.PermissionDeniedException pde) {
            throw new org.osid.OperationFailedException(pde);
        }

        net.okapia.osid.jamocha.adapter.federator.calendaring.FederatingEventLookupSession session =
            new net.okapia.osid.jamocha.adapter.federator.calendaring.FederatingEventLookupSession(calendar);

        for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
            if (mgr.supportsEventLookup() && mgr.supportsVisibleFederation()) {
                try {
                    session.addSession(mgr.getEventLookupSessionForCalendar(calendarId));
                } catch (org.osid.NotFoundException nfe) {
                    // calendarId doesn't exist there
                }
            }
        }

        return (session);        
    }


    /**
     *  Tests if an event query service is supported. An event query service 
     *  defines methods to access events. 
     *
     *  @return true if event query is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsEventQuery() {
        for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
            if (mgr.supportsEventQuery()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service. 
     *
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySession()
        throws org.osid.OperationFailedException {

        /*
         * I don't see any helpful plug-and-play tools, so I guess we
         * build our own.
         */

        FederatingEventQuerySession session = new FederatingEventQuerySession(CALENDAR);

        for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
            if (mgr.supportsEventQuery()) {
                session.addSession(mgr.getEventQuerySession());
            }
        }

        return (session);                
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        if (CALENDAR.getId().equals(calendarId)) {
            return (getEventQuerySession());
        }

        org.osid.calendaring.Calendar calendar;
        try {
            calendar = getCalendarLookupSession().getCalendar(calendarId);
        } catch (org.osid.PermissionDeniedException pde) {
            throw new org.osid.OperationFailedException(pde);
        }

        FederatingEventQuerySession session = new FederatingEventQuerySession(calendar);

        for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
            if (mgr.supportsEventQuery() && mgr.supportsVisibleFederation()) {
                try {
                    session.addSession(mgr.getEventQuerySessionForCalendar(calendarId));
                } catch (org.osid.NotFoundException nfe) {
                    // calendarId doesn't exist there
                }
            }
        }

        return (session);        
    }


    /**
     *  Tests if a calendar lookup service is supported. 
     *
     *  @return <code> true </code> if calendar lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarLookup() {
        return (true); /* we always have one of our own */
    }

    
    /**
     *  Gets the OsidSession associated with the calendar lookup service. 
     *
     *  @return a <code> CalendarLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarLookupSession getCalendarLookupSession()
        throws org.osid.OperationFailedException {

       net.okapia.osid.jamocha.adapter.federator.calendaring.FederatingCalendarLookupSession session =
           new net.okapia.osid.jamocha.adapter.federator.calendaring.FederatingCalendarLookupSession();

       /* don't forget ours */
       session.addSession(new net.okapia.osid.jamocha.core.calendaring.InvariantMapCalendarLookupSession(CALENDAR));

       /*
        * What if this OsidSession is not supported but don't we want
        * to see one anyway?  I may wish to solve here (create a
        * Calendar that maps to the whole provider) or address in a
        * separate adapter.
        *
        * Because the folks at TomCal figured it out by e03, I don't
        * have to worry about it (yet).
        */

       for (org.osid.calendaring.CalendaringManager mgr : this.providers.values()) {
           if (mgr.supportsCalendarLookup()) {
               session.addSession(mgr.getCalendarLookupSession());
           }
       }

       return (session);
    }


    /*
     * See e01.
     */

    static class CalendaringProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        CalendaringProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new tom.TomProvider());

            return;
        }
    }
}

