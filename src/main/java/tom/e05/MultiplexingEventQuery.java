
package tom.e05;

import org.osid.binding.java.annotation.OSID;


/*
 * My first cut was to see about multiplexing every method. I got to
 * the OsidObjectQuery methods and gave up. So, the official position
 * of this class is that it works with select methods and TomCal will
 * take requests for additional methods under consideration.
 */

public final class MultiplexingEventQuery 
    extends net.okapia.osid.jamocha.calendaring.event.spi.AbstractEventQuery
    implements org.osid.calendaring.EventQuery {

    private final java.util.Map<org.osid.calendaring.EventQuerySession, org.osid.calendaring.EventQuery> queries = new java.util.HashMap<>();

    
    /*
     * This maintains an association between the OSID Provider's
     * EventQuerySession and EventQuery. Later, we need to execute the
     * EventQuery in the correct EventQuerySession.
     */

    protected void addEventQuery(org.osid.calendaring.EventQuerySession session,
                                 org.osid.calendaring.EventQuery query) {
        
        this.queries.put(session, query);
        return;
    }


    protected org.osid.calendaring.EventQuery getEventQueryBySession(org.osid.calendaring.EventQuerySession session) {
        return (this.queries.get(session));
    }

    
    /**
     *  Gets the string matching types supported. A string match type 
     *  specifies the syntax of the string query, such as matching a word or 
     *  including a wildcard or regular expression. 
     *
     *  @return a list containing the supported string match types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStringMatchTypes() {
        java.util.Collection<org.osid.type.Type> set = new java.util.HashSet<>();
     
        /*
         * You were waiting for me to resort to buffering an OsidList.
         */

        for (org.osid.calendaring.EventQuery query : this.queries.values()) {
            try (org.osid.type.TypeList types = query.getStringMatchTypes()) {
                while (types.hasNext()) {
                    try {
                        set.add(types.getNextType());
                    } catch (org.osid.OperationFailedException oe) {
                        // nothing I can do about that here
                    }
                }
            }
        }

        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(set));
    }

    
    /**
     *  Tests if the given string matching type is supported. 
     *
     *  @param  stringMatchType a <code> Type </code> indicating a string 
     *          match type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stringMatchType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
        
        /* 
         * This shows that something is supported if it is supported
         * by all providers. Which way is better? Supported by at
         * least one or supported by all? 
         */

        for (org.osid.calendaring.EventQuery query : this.queries.values()) {
            if (!supportsStringMatchType(stringMatchType)) {
                return (false);
            }
        }

        return (true);
    }


    /**
     *  Adds a keyword to match. Multiple keywords can be added to perform a 
     *  boolean <code> OR </code> among them. A keyword may be applied to any 
     *  of the elements defined in this object such as the display name, 
     *  description or any method defined in an interface implemented by this 
     *  object. 
     *
     *  @param  keyword keyword to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> keyword </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> keyword </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchKeyword(String keyword, 
                             org.osid.type.Type stringMatchType, boolean match) {

        for (org.osid.calendaring.EventQuery query : this.queries.values()) {
            /*
             * will eat mismatched types for now
             */
            try {
                matchKeyword(keyword, stringMatchType, match);
            } catch (org.osid.UnsupportedException ue) {
            }
        }

        return;
    }


    /**
     *  Clears all keyword terms. 
     */

    @OSID @Override
    public void clearKeywordTerms() {
        for (org.osid.calendaring.EventQuery query : this.queries.values()) {
            query.clearKeywordTerms();
        }

        return;
    }


    /**
     *  Matches any object. 
     *
     *  @param  match <code> true </code> to match any object <code> , </code> 
     *          <code> false </code> to match no objects 
     */

    @OSID @Override
    public void matchAny(boolean match) {
        for (org.osid.calendaring.EventQuery query : this.queries.values()) {
            query.matchAny(match);
        }

        return;
    }


    /**
     *  Clears the match any terms. 
     */

    @OSID @Override
    public void clearAnyTerms() {
        for (org.osid.calendaring.EventQuery query : this.queries.values()) {
            query.clearAnyTerms();
        }

        return;
    }
}