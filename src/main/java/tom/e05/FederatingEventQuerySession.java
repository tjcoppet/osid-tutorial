
package tom.e05;

import org.osid.binding.java.annotation.OSID;


public final class FederatingEventQuerySession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.EventQuerySession>
    implements org.osid.calendaring.EventQuerySession {

    private final org.osid.calendaring.Calendar calendar;


    FederatingEventQuerySession(org.osid.calendaring.Calendar calendar) {
        this.calendar = calendar;
        return;
    }


    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.EventQuerySession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Tests if this user can perform <code>Event</code> 
     *  queries.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canSearchEvents() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            if (session.canSearchEvents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  A normalized view uses a single <code> Event </code> to represent a
     *  set of recurring events.
     */

    @OSID @Override
    public void useNormalizedEventView() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            session.useNormalizedEventView();
        }

        return;
    }


    /**
     *  A denormalized view expands recurring events into a series of <code>
     *  Events. </code>
     */

    @OSID @Override
    public void useDenormalizedEventView() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            session.useDenormalizedEventView();
        }

        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  events.
     */

    @OSID @Override
    public void useSequesteredEventView() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            session.useSequesteredEventView();
        }
        
        return;
    }
    
    
    /**
     *  All events are returned including sequestered events.
     */
    
    @OSID @Override
    public void useUnsequesteredEventView() {
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            session.useUnsequesteredEventView();
        }
        
        return;
    }
    
    
    /**
     *  Gets an event query.
     *
     *  @return the event query
     */
    
    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
 
        /*
         * Uh oh. Which query will go with what provider? When in
         * doubt, create an object.
         */

        MultiplexingEventQuery query = new MultiplexingEventQuery();
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            query.addEventQuery(session, session.getEventQuery());
        } 

        return (query);

        /*
         * I suppose I have to figure out how to implement this
         * MultiplexingQuery things.  
         */
    }

    
    /**
     *  Gets a list of <code> Events </code> matching the given event query.
     *
     *  @param  eventQuery the event query
     *  @return the returned <code> EventList </code>
     *  @throws org.osid.NullArgumentException <code> eventQuery
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.UnsupportedException <code> eventQuery </code> is not
     *          of this service
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByQuery(org.osid.calendaring.EventQuery eventQuery)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        if (!(eventQuery instanceof MultiplexingEventQuery)) {
            throw new org.osid.UnsupportedException("query not supported");
        }

        MultiplexingEventQuery query = (MultiplexingEventQuery) eventQuery;

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.ParallelEventList ret = new net.okapia.osid.jamocha.adapter.federator.calendaring.event.ParallelEventList();
        
        for (org.osid.calendaring.EventQuerySession session : getSessions()) {
            ret.addEventList(session.getEventsByQuery(query.getEventQueryBySession(session)));
        }

        ret.noMore();
        return (ret);
    }
}
