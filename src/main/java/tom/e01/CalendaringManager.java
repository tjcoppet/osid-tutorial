
package tom.e01;

/**
 * This is a starter implementation of org.osid.calendaring.CalendaringManager.
 */

public final class CalendaringManager 
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCalendaringManager
    implements org.osid.calendaring.CalendaringManager,
               org.osid.calendaring.CalendaringProxyManager {

    private static final String ID           = "urn:osid:tom.net:identifiers:providers:calendaring:e01";
    private static final String DISPLAY_NAME = "TomCal";
    private static final String DESCRIPTION  = "My first OSID Calendaring Provider. I'm not sure what this will do yet.";
    private static final String VERSION      = "0.0.1";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901234Z";

    public CalendaringManager() {
        super(new CalendaringProvider());
        return;
    }

   
    /**
     * Using the ServiceProvider tool to help with setup. OsidManagers
     * need:
     *
     *     serviceId:   A unique identifier for this OSID Provider. I'm
     *                  using the URN scheme (see above).
     *     displayName: A name for this OSID Provider.
     *     description: A description of what this OSID Provider does.
     *     version:     The version of this OSID Provider implementation.
     *     releaseDate: The date in which this version of the OSID
     *                  Provider was released. In my case, I have no idea.
     */

    static class CalendaringProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        CalendaringProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new tom.TomProvider());

            return;
        }
    }
}

