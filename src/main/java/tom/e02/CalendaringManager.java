
package tom.e02;

import org.osid.binding.java.annotation.OSID;
import java.util.*;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  This version includes a data source (GameData) and gets an
 *  EventLookupSession off the ground.
 */

public final class CalendaringManager 
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCalendaringManager
    implements org.osid.calendaring.CalendaringManager,
               org.osid.calendaring.CalendaringProxyManager {

    private static final String ID           = "urn:osid:tom.net:identifiers:providers:calendaring:e02";
    private static final String DISPLAY_NAME = "TomCal";
    private static final String DESCRIPTION  = "My second OSID Calendaring Provider. This will do something.";
    private static final String VERSION      = "0.0.2";
    private static final String RELEASE_DATE = "2017-08-24T08:59:59.3456734568901235Z";

    /* next generation rdbms */
    private final Collection<org.osid.calendaring.Event> eventDB = new ArrayList<>();


    public CalendaringManager() {
        super(new CalendaringProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidRuntime </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        /* stashes the runtime so I can take a few short cuts later */
        super.initialize(runtime);

        populateEvents();

        return;
    }
   

    /**
     *  Tests if an event lookup service is supported. An event lookup service 
     *  defines methods to access events. 
     *
     *  @return true if event lookup is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsEventLookup() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service. 
     *
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSession()
        throws org.osid.OperationFailedException {
        
        return (new EventLookupSession(new MLBCalendar(), this.eventDB));
    }


    /*
     * Populate the database with Events from our data source.
     */
    
    private void populateEvents() {
        for (GameData game : GameData.values()) {
            org.osid.calendaring.Event event = new MLBGame(game); 
            net.okapia.osid.jamocha.builder.validator.calendaring.event.EventValidator.validateEvent(event);
            this.eventDB.add(event);
        }

        return;
    }


    /*
     * See e01.
     */

    static class CalendaringProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        CalendaringProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(new tom.TomProvider());

            return;
        }
    }
}

