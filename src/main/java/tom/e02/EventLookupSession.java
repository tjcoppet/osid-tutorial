
package tom.e02;

import org.osid.binding.java.annotation.OSID;


public final class EventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapEventLookupSession
    implements org.osid.calendaring.EventLookupSession {

  
    protected EventLookupSession(org.osid.calendaring.Calendar calendar, java.util.Collection<org.osid.calendaring.Event> events) {
        setCalendar(calendar);
        putEvents(events);
        return;
    }
}
