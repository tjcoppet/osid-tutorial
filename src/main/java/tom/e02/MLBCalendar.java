package tom.e02;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


/**
 *  Defines a Calendar OsidCatalog.
 */

public final class MLBCalendar
    extends net.okapia.osid.jamocha.calendaring.calendar.spi.AbstractCalendar
    implements org.osid.calendaring.Calendar {


    public MLBCalendar() {
        current();
        setId(new net.okapia.osid.primordium.id.BasicId("tom.net", "osid.calendaring.Calendar", "MLB"));
        setDisplayName(text("Major Leage Baseball"));
        setDescription(text("Game schedule for Major League Baseball."));

        return;
    }
}



